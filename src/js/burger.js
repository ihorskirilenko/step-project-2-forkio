document.addEventListener("DOMContentLoaded", function () {
	let headerBurger = document.querySelector(".header__burger");
	let headerMenu = document.querySelector(".header__menu");
	let headerItem = document.querySelectorAll(".header__item");



	document.addEventListener("click", function (ev) {

		if (ev.target.classList.contains("header__burger") || ev.target.parentElement.classList.contains("header__burger")) {
			headerBurger.classList.toggle("active");
			headerMenu.classList.toggle("active");
			headerItem.forEach(element => {
				element.classList.toggle("active");
			});
		}

		else if (ev.target.classList.contains("header__item") || !ev.target.classList.contains("header__item")) {
			removeClass();
		}
	})

	window.addEventListener("resize", () => removeClass());
	window.addEventListener("scroll", () => removeClass());

	function removeClass() {
		headerBurger.classList.remove("active");
		headerMenu.classList.remove("active");
		headerItem.forEach(el => {
			el.classList.remove("active");
		})
	}
})

