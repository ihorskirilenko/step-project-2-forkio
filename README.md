# Step project #2 - "Forkio"

# About the team

<b>Our team name - "Experience"!</b> <br><br>
Oleksii Slavikovskyi - front-end senior SE <br>
```
Header section
Burger menu
About section
```

Igor Kyrylenko - front-end senior SE
```
Revolutionary editor section
Overview section
Pricing section
```

# Links to project on our sites

http://kyrylen.co/ <br>
http://slavikovskyi.pro/

## Download, install & run project

https://gitlab.com/ihorskirilenko/step-project-2-forkio/

```
git clone https://gitlab.com/ihorskirilenko/step-project-2-forkio.git
npm i
gulp build
gulp dev
```

## Available gulp tasks

gulp clear: removes /dist folder content <br>
gulp css: build css from scss <br>
gulp js: minifying and concat js <br>
gulp img: minifying images <br>
<br>
gulp build: executes gulp clear, css, js, img tasks <br>
gulp dev: calls browser-sync, online rebuilds css, js

# Technologies used:

## Core technologies

HTML<br>
CSS<br>
Vanilla JS<br>
github-buttons npm plugin<br>

## Dev technologies

gulp<br>
gulp-sass<br>
gulp-concat<br>
gulp-autoprefixer<br>
gulp-clean-css<br>
gulp-clean<br>
gulp-purgecss<br>
gulp-uglify<br>
browser-sync<br>
gulp-imagemin<br>

